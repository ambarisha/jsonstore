from setuptools import setup, find_packages
 
setup(
    name='jsonstore',    			# PyPI-package name.
    author='Ambarisha B',
    author_email='ambarisha@emptycup.in',
    version='0.1',                          	# Update the version number for new releases
    install_requires=[
        'requests',
    ],
    packages=find_packages()			# Packages installed by the project
)
