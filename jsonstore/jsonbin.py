import requests


class JsonStore(object):
    _endpoint = 'https://api.jsonbin.io/b'

    def __init__(self, secret, rootkey=None):
        self._secret = secret
        self._rootkey = rootkey if rootkey else self._post_value({'___formality': 'empty json not allowed'})
        self._index = None
        self._load_index()
        
    def get_root_key(self):
        return self._rootkey

    def _load_index(self):
        self._index = self._get_value(self._rootkey)

    def _get_value(self, kid):
        r = requests.get(self._endpoint + '/' + kid + '/latest', headers={
            'secret-key': self._secret,
            'content-type': 'application/json'
        })

        if not r.ok:
            raise ValueError('Failed to get item: Key[%s]' % kid)
        return r.json()

    def get_item(self, key):
        if key not in self._index:
            self._load_index()
            if key not in self._index:
                raise KeyError(key)

        return self._get_value(self._index[key])

    def _put_value(self, kid, value):
        r = requests.put(self._endpoint + '/' + kid, json=value, headers={
            'secret-key': self._secret,
            'content-type': 'application/json',
        })

        if not r.ok:
            raise ValueError('Failed to update item: Key[%s]' % kid)

    def _post_value(self, value):
        r = requests.post(self._endpoint, json=value, headers={
            'secret-key': self._secret,
            'content-type': 'application/json',
            'private': 'true'
        })

        if not r.ok:
            raise ValueError('Failed to save item. StatusCode[%s]' % r.status_code)

        return r.json()['id']

    def _save_index(self):
        self._put_value(self._rootkey, self._index)

    def put_item(self, key, value):
        if key not in self._index:
            self._load_index()
            if key not in self._index:
                jid = self._post_value(value)
                self._index[key] = jid
                self._save_index()
                return

        self._put_value(self._index[key], value)

