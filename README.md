

A simple python client library to use [jsonbin.io](https://jsonbin.io) as key vaue store.

###### Installation

```bash
pip install git+https://bitbucket.org/ambarisha/jsonstore.git
```


###### Getting started:

Sign up on [jsonbin.io](https://jsonbin.io) and get your secret key.

You can create a new store by doing:

```python
from jsonstore import JsonStore

store = JsonStore(secret)
```

Now, you can add or remove items into the store using:

```python
store.put_item('key1', {'hello': 'world'})
val = store.get_item('key1')
```

Also, remember to get the _rootkey_ fo the store so that you can access it later. You can do this by:

```python
rootkey = store.get_root_key()
```

Next time, you can "connect" to this store by doing:

```python
store = JsonStore(secret, rootkey)
```

Todo: Delete not implemented yet.

